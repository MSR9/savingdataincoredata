//
//  Person+CoreDataProperties.swift
//  SavingDataInCoreData
//
//  Created by EPITADMBP04 on 3/29/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//
//

import Foundation
import CoreData


extension Person {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Person> {
        return NSFetchRequest<Person>(entityName: "Person")
    }

    @NSManaged public var name: String?
    @NSManaged public var age: Int16

}
