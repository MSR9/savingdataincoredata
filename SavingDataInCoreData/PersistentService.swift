//
//  File.swift
//  SavingDataInCoreData
//
//  Created by EPITADMBP04 on 3/29/20.
//  Copyright © 2020 PrahladReddy. All rights reserved.
//

import Foundation
import CoreData


   
class PersistentService {

    private init() {}
    
    static var context: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    // MARK: - CoreDataStack
static var persistentContainer: NSPersistentContainer = {
    let container = NSPersistentContainer(name: "SavingDataInCoreData")
    container.loadPersistentStores(completionHandler: { (storeDescription, error) in
        if let error = error as NSError? {
            fatalError("Unresolved error \(error.userInfo)")
        }
    })
    return container
}()
    
    // MARK: - Core Data Saving Support
   static func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
                print("SAVED")
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    
}

